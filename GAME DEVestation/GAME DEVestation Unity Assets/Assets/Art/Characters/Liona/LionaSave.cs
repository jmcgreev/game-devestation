﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LionaSave : MonoBehaviour
{

    [SerializeField]
    private GameObject savePanel;
    private bool canSave;
    public GameManager gameManager;
    NPC npc;

    private void Start()
    {
        savePanel.gameObject.SetActive(false);
        canSave = false;
        npc = GetComponent<NPC>();
    }

    private void Update()
    {
        if (canSave)
        {
            if (Input.GetKey(KeyCode.E))
            {
                Debug.Log("Player Pressed E");
                gameManager.SavePlayer();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        //canSave = true; // uncomment to enable saving through pressing e

        if (collision.CompareTag("Player"))
        {
            Debug.Log("Player Entered");

            npc.TriggerSaveSentence();
            savePanel.gameObject.SetActive(true);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        canSave = false;

        if (collision.CompareTag("Player"))
        {
            Debug.Log("Player Exitted");

            savePanel.gameObject.SetActive(false);
        }

    }
}
