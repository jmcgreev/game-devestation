﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    Scene curScene;

    Transform cam;
    GameObject player;
    PlayerStats playerStats;
    PlayerController playerController;
    EnemyProjectile enemyProjectileScript;

    public bool camFollowingPlayer;
    public bool camConstMove;

    public float camSpeed;


    private void Awake()
    {
        GetPlayer();
        if (GameObject.Find("PlayerSpawn") != null)
        { player.transform.position = GameObject.Find("PlayerSpawn").transform.position; }
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Application.persistentDataPath);

        curScene = SceneManager.GetActiveScene();

        if (curScene.buildIndex != 0)
        {
            cam = GameObject.Find("CameraHolder").transform;
            

            if (enemyProjectileScript != null)
            {
                enemyProjectileScript = GameObject.Find("Enemy").GetComponent<EnemyProjectile>();
            }

            if (curScene.name != "Level3 v1")
            {
                CamFollowsPlayer();
            }
            else
            {
                ConstantMovingLevel();
            }

        }

        //Debug.Log("at player position");
    }

    // Update is called once per frame
    void Update()
    {


        if (camFollowingPlayer)
        {
            cam.Translate(player.transform.position - cam.position);
        }

        if (camConstMove)
        {
            cam.transform.position += Vector3.right * Time.deltaTime * camSpeed;
        }

        //Reset Scene
        if (Input.GetKeyDown("r"))
        {
            ReloadLevel();
        }



    }


    public void ConstantMovingLevel()
    {
        camFollowingPlayer = false;
        camConstMove = true;
    }
    public void CamFollowsPlayer()
    {
        camFollowingPlayer = true;
        camConstMove = false;
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    //save

    public void SavePlayer()
    {
        SaveSystem.SavePlayer(playerStats);
    }

    public void SavePlayer2()
    {
        SaveSystem.SavePlayer2(playerStats);
    }

    public void SavePlayer3()
    {
        SaveSystem.SavePlayer3(playerStats);
    }

    //load

    public void LoadEverything()
    {

        LoadScene();
        LoadPlayer();

    }

    public void LoadEverything2()
    {

        LoadScene();
        LoadPlayer2();

    }

    public void LoadEverything3()
    {

        LoadScene();
        LoadPlayer3();

    }

    public void LoadScene()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        SceneManager.LoadScene(data.level);

    }



    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        GetPlayer();

        playerStats.curHealth = data.health;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

        player.transform.position = position; // this has x,y,z position of the player



    }

    public void LoadPlayer2()
    {
        PlayerData data = SaveSystem.LoadPlayer2();
        GetPlayer();

        playerStats.curHealth = data.health;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

        player.transform.position = position; // this has x,y,z position of the player

    }

    public void LoadPlayer3()
    {
        PlayerData data = SaveSystem.LoadPlayer3();
        GetPlayer();

        playerStats.curHealth = data.health;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

        player.transform.position = position; // this has x,y,z position of the player

    }

    void GetPlayer()
    {
        player = GameObject.Find("Player");
        playerStats = player.GetComponent<PlayerStats>();
        playerController = player.GetComponent<PlayerController>();
    }

}
