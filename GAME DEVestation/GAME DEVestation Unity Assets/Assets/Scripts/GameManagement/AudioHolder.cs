﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHolder : MonoBehaviour
{
    AudioSource[] audios;
    AudioManager AM;

    AudioClip level;  //1st audiosource
    AudioClip combat;  //2nd audiosource
    AudioClip boss;     //3rd audiosource
    public AudioClip death;    //4th audiosource

    private void Awake()
    {
        audios = GetComponents<AudioSource>();
        AM = FindObjectOfType<AudioManager>();
        if (audios[0] != null)
        {
            level = audios[0].clip;
        }
        if (audios[1] != null)
        {
            combat = audios[1].clip;
        }
        if (audios[2] != null)
        {
            boss = audios[2].clip;
        }
        if (audios[3] != null)
        {
            death = audios[3].clip;
        }
    }

    private void Start()
    {
        PlayLevelMusic();
    }

    public void PlayLevelMusic()
    {
        AM.PlayLevelMusic(level);
    }

    public void PlayCombatMusic()
    {
        AM.PlayCombatMusic(combat);
    }

    public void PlayBossMusic()
    {
        AM.PlayBossMusic(boss);
    }

    public void PlayDeathMusic()
    {
        AM.PlayDeathMusic(death);
    }

    public void TurnOffMusic(int clipNum)
    {
        AM.TurnOffMusic(clipNum);
    }

}
