﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable] //Use this attribute to allow this data to be saved within a file
public class PlayerData   //Delete MonoBehavior to not let it be a component of the scene
{

    public int health;        // int for HP
    public int level;         // hopefully this allows scene changing
    public float[] position;  // Float array for player position Vector3 x,y,z

    public PlayerData(PlayerStats Stats)  //Created a struct to store the player's data from above^
    {
        level = Stats.level;
        health = Stats.curHealth;

        position = new float[3];
        position[0] = Stats.transform.position.x;
        position[1] = Stats.transform.position.y;
        position[2] = Stats.transform.position.z;
    }

}
