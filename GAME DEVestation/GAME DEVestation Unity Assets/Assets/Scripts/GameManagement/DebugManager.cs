﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DebugManager : MonoBehaviour
{
    PanelTransition panelTransition;

    private void Awake()
    {
        panelTransition = FindObjectOfType<PanelTransition>();
    }


    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }


    void Update()
    {
        //DebugUIMenu
        if (Input.GetButtonDown("Pause"))
        {
            panelTransition.OpenPanel();
        }
    }
}
