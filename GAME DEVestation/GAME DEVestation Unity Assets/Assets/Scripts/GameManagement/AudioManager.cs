﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio; //for audiomixersnapshot & TransitionTo

public class AudioManager : MonoBehaviour
{
    public AudioMixerSnapshot combat;
    public AudioMixerSnapshot unpaused;    
    public AudioMixerSnapshot death;    

    public AudioSource[] audios;
    AudioSource backgroundM;
    AudioSource combatM;
    AudioSource bossM;
    AudioSource deathM;

    public AudioClip clipToAdd;

    private float transitionTime;

    private void Awake()
    {
        audios = GetComponents<AudioSource>();
        backgroundM = audios[0];
        combatM = audios[1];
        bossM = audios[1];
        deathM = audios[2];

        transitionTime = 1f;
    }

    void CombatTransition()
    {
        combat.TransitionTo(transitionTime);
    }

    void UnpausedTransition()
    {
        unpaused.TransitionTo(transitionTime);
    }

    void DeathTransition()
    {
        death.TransitionTo(transitionTime);
    }

    public void PlayLevelMusic(AudioClip audioclip)
    {
        clipToAdd = audioclip;
        backgroundM.clip = clipToAdd;
        UnpausedTransition();
        backgroundM.Play();
    }

    public void PlayCombatMusic(AudioClip audioclip)
    {
        clipToAdd = audioclip;
        combatM.clip = clipToAdd;
        CombatTransition();
        combatM.Play();
    }

    public void PlayBossMusic(AudioClip audioclip)
    {
        clipToAdd = audioclip;
        bossM.clip = clipToAdd;
        CombatTransition();
        bossM.Play();
    }

    public void PlayDeathMusic(AudioClip audioclip)
    {
        clipToAdd = audioclip;
        deathM.clip = clipToAdd;
        DeathTransition();
        deathM.Play();
    }

    public void TurnOffMusic(int clipNum)
    {
        audios[clipNum].Stop();
        UnpausedTransition();
    }


}
