﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {
    PlayerController PC;
    public bool isFacingRight;
    Animator animator;
    string previousAnim = "";

	// Use this for initialization
	void Start ()
    {
        PC = GetComponent<PlayerController>();
        animator = GetComponent<Animator>();
        isFacingRight = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (PC.moveHorizontal > 0 && !isFacingRight)
        {
            FlipSprite();          
        }
        else if (PC.moveHorizontal < 0 && isFacingRight)
        {
            FlipSprite();
        }
        else
        {

        }

        
	}

    public void SetAnimationBool(string bName, bool value)
    {
        if (animator != null)
        {
            //previousAnim = animator.get
            animator.SetBool(bName, value);
        }
    }


    public void SetAnimationTrigger(string trigger)
    {
        if (animator != null)
        {
            animator.SetTrigger(trigger);
        }
    }

    void FlipSprite()
    {
        isFacingRight = !isFacingRight;


        Vector3 theScale = transform.localScale;
        theScale.x *= -1;       
        transform.localScale = theScale;
    }
}
