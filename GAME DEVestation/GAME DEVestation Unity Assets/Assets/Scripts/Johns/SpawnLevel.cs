﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLevel : MonoBehaviour
{

    public Transform Spawnpoint; // Art Team can manually move the spawn point for ease of testing
    public GameObject Prefab; // This is the level prefab that the trigger will instantiate


    // When the player enters the trigger, the level will be created as a child of the trigger game object
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Player Entering: Instantiate Level");
        //Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation);
        //(Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation) as GameObject).transform.parent = parentGameObject.transform;
        (Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation) as GameObject).transform.parent = this.transform;

    }

    // When the player exits the trigger, ALL child game objects will be deleted, leaving only the trigger left to be entered later
    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Player Exiting: Destroy Level");
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
