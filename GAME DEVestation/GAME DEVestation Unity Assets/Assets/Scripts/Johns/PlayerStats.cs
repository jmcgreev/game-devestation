﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerStats : MonoBehaviour
{
    public int level;
    public int curHealth = 10;
    public int maxHealth = 10;
    AudioHolder ah;
    bool playDeathSFX;

    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth;
        level = SceneManager.GetActiveScene().buildIndex;
        Debug.Log("Stored level index");
        Debug.Log(level);
        ah = FindObjectOfType<AudioHolder>();

        playDeathSFX = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }

        if (curHealth <= 0 && !playDeathSFX)
        {
            Die();
        }

        //Debug.Log(curHealth);
    }

    void Die()
    {
        playDeathSFX = true;
        StartCoroutine(PlayDeathSFXThenReload());
        // Restart scene
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        // Death animation
        // Game over screen
    } 

    public void Damage(int dmg)
    {
        curHealth -= dmg;
    }

    IEnumerator PlayDeathSFXThenReload()
    {
        ah.PlayDeathMusic();
        yield return new WaitForSeconds(ah.death.length);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
