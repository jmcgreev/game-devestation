﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerberusHealth : MonoBehaviour
{
    /*
     * This is the basically the same as EnemyHealth.cs but tailored specifically for the cerberus boss
     * I changed canMove to isAlive because cerberus needs to be able to both stop on the edges (canMove being false) 
     * and die (isAlive being false);
    */

    public int HP = 50;
    int curHP;

    public bool canBeHit;
    float TimeTillTakeDMG = 0.0f;
    float resetTime = 1.1f;
    Cerberus puppy; // This is pretty much the only thing I changed since the script works fine

    // Start is called before the first frame update
    void Start()
    {
        puppy = GetComponent<Cerberus>();
        curHP = HP;
        canBeHit = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (HP != curHP) //Checks that enemy has taken damage
        {

            TimeTillTakeDMG = resetTime; //Sets the timer till can take damage again
            curHP = HP;
            canBeHit = false;
            //puppy.isAlive = false; // I commented isAlive out, because it didn't seem like it did anything. I t also could cause problems later cause its technically dead in this statement
        }

        if (TimeTillTakeDMG > 0) //if timer is a non-zero num then reduce timer
        {
            TimeTillTakeDMG -= Time.deltaTime;
        }
        if (TimeTillTakeDMG <= 0)  //resets timer if has dropped to zero or below
        {
            TimeTillTakeDMG = 0.0f;
            canBeHit = true;
            //puppy.isAlive = true;
        }

    }

    void OnCollisionEnter2D(Collision2D other) //Current boss damage check for collision with GO tagged with "PlayerDMG", 
    {                                                         //then receives damage based on a received Int from damage delt by GO
        if (other.gameObject.tag == "PlayerDMG")
        {
            
            HP -= other.gameObject.GetComponent<PlayerAttack>().swrdDMG;
        }
        else
        {

        }
    }
}
