﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHide : MonoBehaviour
{

    public float alphaLevel = 1.0f;

    private void Update()
    {
        GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, alphaLevel);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("player entered");
        alphaLevel = 0f;

    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("player exitted");
        alphaLevel = 1.0f;
    }

}
