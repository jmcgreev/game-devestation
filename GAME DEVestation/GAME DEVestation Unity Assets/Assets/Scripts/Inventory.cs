﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
        }
        
        instance = this;
    }

    #endregion

    public delegate void OnItemChanged();   //An event that you can subscribe different methods to, if you trigger the event then all methods will be called
    public OnItemChanged OnItemChagnedCallback;

    public int space = 20;

    public List<Item> items = new List<Item>();

    public bool Add (Item item)
    {
        if (!item.isDefaultItem)
        {
            if (items.Count >= space)
            {
                Debug.Log("Not enough space!");
                return false;
            }
            items.Add(item);

            if (OnItemChagnedCallback != null)  //this checks if there is an event to trigger
            {
                OnItemChagnedCallback.Invoke(); // this means we are triggering the event
            }
        }

        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);

        if (OnItemChagnedCallback != null)
        {
            OnItemChagnedCallback.Invoke();
        }
    }

}
