﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelTransition : MonoBehaviour
{
    public GameObject Panel;
    bool isOpen;

    public void OpenPanel()
    {
        if (Panel != null)
        {
            Animator animator = Panel.GetComponent<Animator>();
            if (animator != null)
            {

                isOpen = animator.GetBool("Open");

                animator.SetBool("Open", !isOpen);
            }
        }
    }




}
