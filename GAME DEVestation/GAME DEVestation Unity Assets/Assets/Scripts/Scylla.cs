﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scylla : MonoBehaviour
{
    public States state;
    public GameObject Player;
    public GameObject Damage;
    BoxCollider2D hurtBox;
    public float timeTillAttack;
    public float incrAtkTime;

    float atkCUTime;
    public float incrCUTime;

    public bool hasAttacked = false;

    public enum States
    {
        Idle,
        Move,
        Attack
    };

    void Awake()
    {
        state = States.Idle;
        //Player = GameObject.Find("Player");

        hurtBox = Damage.GetComponent<BoxCollider2D>();
    }

    void Start()
    {
        timeTillAttack = 0.0f;               
        ResetAttackTimer();
    }

    void Update()
    {
        if(timeTillAttack <= 0.0f)
        {state = States.Attack;}

        if(atkCUTime <= 0.0f && hasAttacked)
        { AttackCleanup(); }

        if (state == States.Attack)
        {
            //Do Attack 
            if (!hasAttacked)
            {
                Attack();
            }
        }

        /*
        else if (timeTillAttack > 0.0f)
        {
            state = States.Move;

            //Worry about move stuff later
        }
        */
        if (timeTillAttack > 0)
        {
            timeTillAttack -= Time.deltaTime;
        }
        if(atkCUTime > 0)
        {
            atkCUTime -= Time.deltaTime;
        }
    }

    void Attack()
    {
        ScyllaDamage SD = FindObjectOfType<ScyllaDamage>();
        hasAttacked = true;
        SD.GetPlayerPosition();
        SD.AcquiringPlayer();
        hurtBox.enabled = true;
        ResetAttackCUTimer();
    }

    void AttackCleanup()
    {
        Debug.Log("cleanUP");
        state = States.Idle;
        hasAttacked = false;
        ResetHurtBox();
        ResetAttackTimer();
    }

    float ResetAttackTimer()
    {        
        return timeTillAttack = incrAtkTime;
    }
    float ResetAttackCUTimer()
    {        
        return atkCUTime = incrCUTime;
    }


    void ResetHurtBox()
    {
        hurtBox.enabled = false;
        Damage.transform.position = GetComponentInParent<Transform>().position;
    }

}
