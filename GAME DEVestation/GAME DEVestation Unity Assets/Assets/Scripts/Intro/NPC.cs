﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerSaveSentence()
    {
        FindObjectOfType<DialogueManager>().StartSentence(dialogue);
    }
}
