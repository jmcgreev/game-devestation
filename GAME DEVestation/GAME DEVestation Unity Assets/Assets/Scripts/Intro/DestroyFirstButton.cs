﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFirstButton : MonoBehaviour
{
    public void DeleteThisButton()
    {
        Destroy(gameObject); // This is only because brackeys used 2 buttons and I only want to use 1, so the first button is deleted after starting the intro
    }
}
