﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroTrigger : MonoBehaviour
{

    public Intro intro;

    public void TriggerIntro()
    {
        FindObjectOfType<IntroManager>().StartIntro(intro);
    }
}
