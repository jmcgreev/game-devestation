﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public float textSpeed;
    public Text text;
    private Queue<string> sentences;

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartSentence(Dialogue dialogue)
    {
        sentences.Clear();  // Clears the current queue if there was a previous conversation

        foreach (string sentence in dialogue.sentences)  //Add each sentence from NPC dialogue to the queue
        {
            sentences.Enqueue(sentence);
        }        
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            return;
        }

        // This is for when there's still sentences that need to be displayed
        string sentence = sentences.Dequeue();
        StopAllCoroutines(); // if a coroutine is already running, stop it. (this is for spam-clicking through the intro)
        StartCoroutine(TypeSentence(sentence)); // Calling a coroutine so that we can "animate" the letters appearing the UI

    }

    public IEnumerator TypeSentence(string sentence)
    {
        text.text = "";
        
        foreach(char letter in sentence)
        {
            text.text += letter;
            yield return new WaitForSeconds(textSpeed);
        }        
    }




}
