﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour
{

    public Text introText;
    public Image introImage;
    public Animator animator;
    private Queue<string> sentences;

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartIntro(Intro intro)
    {
        animator.SetBool("IsVisible", true); // Set it to true in the animator so the intro fades in

        sentences.Clear(); // This is to make sure that there's no hold-over sentences from previous intros (somehow)

        foreach (string sentence in intro.sentences)
        {
            sentences.Enqueue(sentence); // This will loop through all the sentenses and incrimentally display them
        }

        ////////////////////// Might ignore this later////////////////////////////////
        ///

        foreach (Sprite sprite in intro.spriteList)
        {
            
        }

        /////////////////////////////////////////////////////////////////////////////

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if(sentences.Count == 0)
        {
            // This means we've reached the end of the queue so the intro can be over
            EndIntro();
            return;
        }

        // This is for when there's still sentences that need to be displayed
        string sentence = sentences.Dequeue();
        StopAllCoroutines(); // if a coroutine is already running, stop it. (this is for spam-clicking through the intro)
        StartCoroutine(TypeSentence(sentence)); // Calling a coroutine so that we can "animate" the letters appearing the UI

    }

    IEnumerator TypeSentence (string sentence)
    {
        introText.text = ""; // Starting an empty string
        // This will convert the strings into character arrays so that the letters of each sentence will come out sequentially
        foreach (char letter in sentence.ToCharArray())
        {
            introText.text += letter; // This will append each letter to the end of the string (starting empty)
            yield return null; // This means that the computer will only wait a single frame between each letter
        }
    }

    void EndIntro()
    {
        animator.SetBool("IsVisible", false); // Set it to false in the animator so it fades out
    }


}
