﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Intro
{

    public Sprite[] spriteList;

    [TextArea(1, 2)] // This takes 2 ints with the (minimum lines) and (maximum lines)
    public string[] sentences;
}
