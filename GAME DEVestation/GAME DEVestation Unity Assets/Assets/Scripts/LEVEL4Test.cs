﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LEVEL4Test : MonoBehaviour
{
    public string level;

    public void NextScene()
    {
        SceneManager.LoadScene(level);
    }
}
