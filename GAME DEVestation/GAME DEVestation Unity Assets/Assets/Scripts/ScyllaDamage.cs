﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScyllaDamage : MonoBehaviour
{
    Vector3 ScyllaPos;
    public float pullSpeed;
    public float acquireSpeed;
    public float pullRange;
    public bool bIsPulling = false;
    public bool bAcquirePlayer = false;   
    GameObject Player;
    Vector3 lastPlayerPos;
    Vector3 targetPos;
    void Awake()
    {
        ScyllaPos = GetComponentInParent<Transform>().position;
        Debug.Log(ScyllaPos);
        Player = FindObjectOfType<Scylla>().Player;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            //Deal Damage to player & initiate pull towards Scylla
            //Damage();
            AcquiringPlayer();
            IsPulling();
            Player.GetComponent<PlayerController>().CanMove();            
        }
    }

    public Vector3 GetPlayerPosition()
    {
        lastPlayerPos = Player.transform.position;
        return lastPlayerPos;
    }

    bool IsPulling()
    {        
        return bIsPulling = !bIsPulling;
    }
    public bool AcquiringPlayer()
    {       
        return bAcquirePlayer = !bAcquirePlayer;
    }

    private void Update()
    {        
        if (bAcquirePlayer)
        {
            gameObject.transform.Translate((lastPlayerPos - gameObject.transform.position) * acquireSpeed * Time.deltaTime);
            if (gameObject.transform.localPosition == Player.transform.position)
            {
                bAcquirePlayer = false;
                bIsPulling = true;
            }
            else if(gameObject.transform.localPosition == lastPlayerPos)
            {
                bAcquirePlayer = false;
            }
        }
        
        else if (bIsPulling)
        {
            Player.transform.Translate(ScyllaPos * (Time.deltaTime * pullSpeed));
            if (Mathf.Abs(ScyllaPos.x - Player.transform.position.x) < pullRange)
            {
                bIsPulling = false;
                Player.GetComponent<PlayerController>().CanMove();
            }            
        }
    }
}
