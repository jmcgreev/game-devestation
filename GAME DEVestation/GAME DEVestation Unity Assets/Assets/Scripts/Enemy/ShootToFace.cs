﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootToFace : MonoBehaviour
{

    IEnumerable coroutine;

    GameObject Target;  //Creates target
    Vector3 TargetPos;


    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.Find("Player"); //Sets target to player
        TargetPos = Target.transform.position; //Finds where player pos on spawn
        StartCoroutine(BulletEraseTimer(2.0f));
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate((TargetPos - transform.position) * Time.deltaTime); //Moves projectile to PlayerPos at bullet spawn


    }


    IEnumerator BulletEraseTimer(float decayTime)
    {
        yield return new WaitForSeconds(decayTime);
        Destroy(gameObject);
    }

}
