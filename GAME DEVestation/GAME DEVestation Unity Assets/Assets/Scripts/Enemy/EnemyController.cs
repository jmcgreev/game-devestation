﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed; //needs public value in editor


    Vector2 startPos;
    public Vector2 curPos;


    Vector2 farRightPos; //right-extreme position of walk path
    Vector2 farLeftPos; //left-extreme position of walk path
    public Vector2 maxWalkDist; //needs public value in editor

    public bool atStart;//if at start of walk path
    public bool atRight; //if at right-extreme of walk path
    public bool atLeft; //if at left-extreme of walk path
    public bool isFalling;
    public bool canMove;


    Rigidbody2D enemyRB;


    private GameObject player; //checks player object

    public bool seesPlayer; //tells if seen player
    public float chaseSpeed; //needs public value in editor

    public PlayerController playerControllerScript;
    EnemyProjectile enemyProjectileScript;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        curPos = startPos;
        farRightPos = startPos + maxWalkDist;
        farLeftPos = startPos - maxWalkDist;

        canMove = true;
        atStart = true;
        atRight = false;
        atLeft = false;
        isFalling = false;


        enemyRB = GetComponent<Rigidbody2D>();

        seesPlayer = false;

        player = GameObject.Find("Player");
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        enemyProjectileScript = GetComponent<EnemyProjectile>();
    }

    // Update is called once per frame
    void Update()
    {

 //MOVEMENT LOOP
        curPos = transform.position; //enemy current position resets itself to actually current position


        if (enemyRB.velocity.y < 0)
        {
            atStart = false;
            enemyRB.velocity = new Vector2(enemyRB.velocity.x, enemyRB.velocity.y);
            //Debug.Log(enemyRB.velocity.y);
            isFalling = true;
        }

        else if (isFalling && enemyRB.velocity.y == 0.0f)
        {
            //Debug.Log("On the ground");
            //Debug.Log(enemyRB.velocity.y);
            isFalling = false;
            atStart = true;
        }

        if (canMove)
        { 
            if (!seesPlayer) // if the enemy doesn't see the player in range
            {
                if (atStart) // if at the start position of walk cycle
                {
                    enemyRB.velocity = transform.right * speed; // move the enemy right
                    if (curPos.x >= farRightPos.x) // if at the right extreme stop moving and signify atRight extreme
                    {
                        atStart = false;
                        atRight = true;
                        enemyRB.velocity = Vector2.zero;
                        // Debug.Log("At the right.");
                    }
                }

                if (atRight) // once signified at right extreme start leftward movement
                {
                    enemyRB.velocity = transform.right * speed * -1; // Multiply by neg. 1 for Left direction
                    if (curPos.x <= farLeftPos.x) // if at the left extreme stop moving and signify atLeftt extreme
                    {
                        atRight = false;
                        atLeft = true;
                        enemyRB.velocity = Vector2.zero;
                        // Debug.Log("At the Left.");
                    }
                }

                if (atLeft) // once signified at left extreme start rightward movement
                {
                    enemyRB.velocity = transform.right * speed; // move the enemy right
                    if (curPos.x >= farRightPos.x)
                    {
                        atLeft = false;
                        atRight = true;
                        enemyRB.velocity = Vector2.zero;
                        // Debug.Log("At the Right.");
                    }
                }
            }

            else if (seesPlayer) //If enemy see's player in range go towards them
            {
                transform.position = Vector2.MoveTowards(transform.position, player.transform.position, (chaseSpeed * Time.deltaTime));
                // Debug.Log("Following player, not in loop.");
            }
        }
    }
 
}
