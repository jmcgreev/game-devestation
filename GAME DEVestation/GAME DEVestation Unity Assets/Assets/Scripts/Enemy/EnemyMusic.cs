﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMusic : MonoBehaviour
{
   AudioHolder AH;

    private void Awake()
    {
        AH = FindObjectOfType<AudioHolder>();
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            Debug.Log("Inside Combat Zone");
            AH.PlayCombatMusic();
        }
    }


    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            AH.TurnOffMusic(1);
        }
    }

}
