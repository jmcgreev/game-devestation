﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public GameObject Bullet; //bullet prefab
    public Vector2 spawnBullet; //bullet spawn position

    public Transform target;

    EnemyController enemyControllerScript; //enemy coords

    public float ShootSpeed;

 
    // Start is called before the first frame update
    void Start()
    {
        enemyControllerScript = GetComponent<EnemyController>(); // need to orient bullet spawn off of enemyposition

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot()
    {
        spawnBullet = enemyControllerScript.curPos + new Vector2(0.1f, 0.0f); //BUllet spawn
        GameObject projectile = Instantiate(Bullet, spawnBullet, Quaternion.identity);// Creates bullet from prefab

    }

}
